#import <CoreGraphics/CoreGraphics.h>
#import "CKViewController.h"
#import "CKCalendarView.h"

@implementation CKViewController 


@synthesize dateFormatter = _dateFormatter;
@synthesize ckCalendar;
@synthesize tableView;

- (id)init {
    self = [super init];
    if (self) {
       
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
    
    calendar.delegate = self;
    
    
    calendar.selectedDate = [NSDate date];
    calendar.shouldFillCalendar = YES;
    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    
    

    self.ckCalendar = calendar;
    
    calendar = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(void)heightDidChange{
    NSLog(@"The height of the Calendar View has changed");
}

- (void)localeDidChange {
    [self.ckCalendar setLocale:[NSLocale currentLocale]];
}

#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
//    self.dateLabel.text = [self.dateFormatter stringFromDate:date];
}

- (void)calendar:(CKCalendarView *)calendar didChangeMonth:(NSDate *)date{
    
    self.tableView.frame = CGRectMake(0, CGRectGetMaxY(self.ckCalendar.frame), self.view.bounds.size.width, self.view.bounds.size.height-CGRectGetMaxY(self.ckCalendar.frame));
    [self.tableView reloadData];
    

}



@end