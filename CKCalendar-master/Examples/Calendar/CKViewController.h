#import <UIKit/UIKit.h>
#import "CKCalendarView.h"

@interface CKViewController : UIViewController <CKCalendarDelegate>

@property (nonatomic,strong) IBOutlet CKCalendarView * ckCalendar;



@property (nonatomic,unsafe_unretained)IBOutlet UITableView * tableView;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@end